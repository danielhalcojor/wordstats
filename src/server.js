'use strict';

const compression = require('compression');
const express = require('express');

const PORT = process.env.PORT || 3000;

let app = express();

app.use(compression());
app.use('/node_modules', express.static('node_modules'));
app.use(express.static('src'));

app.get('/*', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.listen(PORT, function () {
  console.log('%d listening on %d', process.pid, PORT);
});
