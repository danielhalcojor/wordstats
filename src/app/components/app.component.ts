import {Component, OnInit} from '@angular/core';
import {FirebaseService} from "../services/firebase.service";

@Component({
  selector: 'wordstats',
  templateUrl: 'app/templates/wordstats.html'
})
export class AppComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
    FirebaseService.initFirebase();
    FirebaseService.initFirebaseUI();
  }

}
