"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var paragraph_1 = require("../models/paragraph");
var ParagraphComponent = (function () {
    function ParagraphComponent() {
        this.onChanged = new core_1.EventEmitter();
    }
    ParagraphComponent.prototype.updateParagraph = function () {
        this.paragraph.updateNumSentences();
        this.paragraph.updateNumWords();
        this.paragraph.updateNumChars();
        this.onChanged.emit();
    };
    return ParagraphComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", paragraph_1.Paragraph)
], ParagraphComponent.prototype, "paragraph", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ParagraphComponent.prototype, "index", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ParagraphComponent.prototype, "onChanged", void 0);
ParagraphComponent = __decorate([
    core_1.Component({
        selector: 'wordstats-p',
        templateUrl: 'app/templates/paragraph.html'
    })
], ParagraphComponent);
exports.ParagraphComponent = ParagraphComponent;
//# sourceMappingURL=paragraph.component.js.map