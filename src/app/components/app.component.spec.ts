import { AppComponent } from './app.component';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import {SummaryComponent} from "./summary.component";
import {ParagraphComponent} from "./paragraph.component";
import {FormsModule} from "@angular/forms";

describe('AppComponent', () => {
  let de: DebugElement;
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ AppComponent, SummaryComponent, ParagraphComponent ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;
    });
  }));

  beforeEach(() => {
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('nav'));
  });

  it('should create component', () => expect(comp).toBeDefined() );

  it('should have expected <nav> text', () => {
    const nav = de.nativeElement;
    expect(nav.innerText).toContain('WordStats');
  });
});
