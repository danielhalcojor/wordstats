const $ = require("jquery");
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Paragraph} from "../models/paragraph";

@Component({
    selector: 'wordstats-main',
    templateUrl: 'app/templates/main.html'
})
export class MainComponent implements OnInit, AfterViewInit {

  paragraphs: Paragraph[];
  totals: {};

  constructor() {
    this.paragraphs = [new Paragraph()];
    this.totals = {
      sentences: 0,
      words: 0,
      chars: 0
    }
  }

  ngOnInit() {
    // Make a clone of the navbar
    let $navbar = $('nav.navbar');
    $navbar.before($navbar.clone().addClass("clone"));

    // When we scroll past the navbar, show the clone fixed to the top of the screen
    $(window).scroll(function() {
      $('body').toggleClass('down', $(this).scrollTop() > 75);
    });

    // TODO remove this temporary initialization
    let firstParagraph = this.paragraphs[0];
    firstParagraph.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sagittis lorem est, vel scelerisque nisl placerat sed. Sed venenatis ex eget neque accumsan mattis. Pellentesque tincidunt congue feugiat. Aenean a eleifend lectus. Pellentesque posuere faucibus lacus ac elementum. Phasellus ut varius est. Vivamus mollis tristique tempus. Duis molestie id metus vitae mattis. Donec at tristique dolor. Curabitur placerat nisl eget mi imperdiet, et laoreet odio vulputate. Cras sit amet urna congue, auctor arcu at, aliquam velit. Cras elementum libero sed arcu auctor lacinia. Maecenas porta arcu vitae felis accumsan commodo. Cras molestie, odio vitae viverra pharetra, tellus sem fermentum tellus.";
    firstParagraph.updateNumSentences();
    firstParagraph.updateNumWords();
    firstParagraph.updateNumChars();
    this.onChanged();
  }

  ngAfterViewInit() {
    // Enable tooltips
    //$('[data-toggle="tooltip"]').tooltip();
  }

  addParagraph() {
    this.paragraphs.push(new Paragraph());
  }

  onChanged() {
    let totalSentences = 0,
      totalWords = 0,
      totalChars = 0;

    this.paragraphs.forEach(paragraph => {
      totalSentences += paragraph.sentences.length;
      totalWords += paragraph.numWords;
      totalChars += paragraph.numChars;
    });

    this.totals = {
      sentences: totalSentences,
      words: totalWords,
      chars: totalChars
    }
  }

}
