import {Component} from '@angular/core';

@Component({
  selector: 'wordstats-info',
  templateUrl: 'app/templates/info.html',
})
export class InfoComponent {}
