import {Component, Input} from '@angular/core';

@Component({
    selector: 'wordstats-summary',
    templateUrl: 'app/templates/summary.html'
})
export class SummaryComponent {
  @Input() totals: {};
}
