import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Paragraph} from "../models/paragraph";

@Component({
    selector: 'wordstats-p',
    templateUrl: 'app/templates/paragraph.html'
})
export class ParagraphComponent {
  @Input() paragraph: Paragraph;
  @Input() index: number;
  @Output() onChanged = new EventEmitter();

  updateParagraph() {
    this.paragraph.updateNumSentences();
    this.paragraph.updateNumWords();
    this.paragraph.updateNumChars();
    this.onChanged.emit();
  }
}
