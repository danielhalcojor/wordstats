"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var $ = require("jquery");
var core_1 = require("@angular/core");
var paragraph_1 = require("../models/paragraph");
var MainComponent = (function () {
    function MainComponent() {
        this.paragraphs = [new paragraph_1.Paragraph()];
        this.totals = {
            sentences: 0,
            words: 0,
            chars: 0
        };
    }
    MainComponent.prototype.ngOnInit = function () {
        // Make a clone of the navbar
        var $navbar = $('nav.navbar');
        $navbar.before($navbar.clone().addClass("clone"));
        // When we scroll past the navbar, show the clone fixed to the top of the screen
        $(window).scroll(function () {
            $('body').toggleClass('down', $(this).scrollTop() > 75);
        });
        // TODO remove this temporary initialization
        var firstParagraph = this.paragraphs[0];
        firstParagraph.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sagittis lorem est, vel scelerisque nisl placerat sed. Sed venenatis ex eget neque accumsan mattis. Pellentesque tincidunt congue feugiat. Aenean a eleifend lectus. Pellentesque posuere faucibus lacus ac elementum. Phasellus ut varius est. Vivamus mollis tristique tempus. Duis molestie id metus vitae mattis. Donec at tristique dolor. Curabitur placerat nisl eget mi imperdiet, et laoreet odio vulputate. Cras sit amet urna congue, auctor arcu at, aliquam velit. Cras elementum libero sed arcu auctor lacinia. Maecenas porta arcu vitae felis accumsan commodo. Cras molestie, odio vitae viverra pharetra, tellus sem fermentum tellus.";
        firstParagraph.updateNumSentences();
        firstParagraph.updateNumWords();
        firstParagraph.updateNumChars();
        this.onChanged();
    };
    MainComponent.prototype.ngAfterViewInit = function () {
        // Enable tooltips
        //$('[data-toggle="tooltip"]').tooltip();
    };
    MainComponent.prototype.addParagraph = function () {
        this.paragraphs.push(new paragraph_1.Paragraph());
    };
    MainComponent.prototype.onChanged = function () {
        var totalSentences = 0, totalWords = 0, totalChars = 0;
        this.paragraphs.forEach(function (paragraph) {
            totalSentences += paragraph.sentences.length;
            totalWords += paragraph.numWords;
            totalChars += paragraph.numChars;
        });
        this.totals = {
            sentences: totalSentences,
            words: totalWords,
            chars: totalChars
        };
    };
    return MainComponent;
}());
MainComponent = __decorate([
    core_1.Component({
        selector: 'wordstats-main',
        templateUrl: 'app/templates/main.html'
    }),
    __metadata("design:paramtypes", [])
], MainComponent);
exports.MainComponent = MainComponent;
//# sourceMappingURL=main.component.js.map