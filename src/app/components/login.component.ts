import {Component, OnInit} from '@angular/core';
import {FirebaseService} from "../services/firebase.service";

@Component({
  selector: 'wordstats-login',
  templateUrl: 'app/templates/login.html'
})
export class LoginComponent implements OnInit {

  user: firebase.User;

  constructor() {
    this.user = null;
  }

  ngOnInit(): void {
    // The start method will wait until the DOM is loaded.
    FirebaseService.setSignInCallback(this.setUser);
    FirebaseService.startFirebaseUI();
  }

  static signOut() {
    FirebaseService.signOut();
  }

  setUser(user: firebase.User, credential: any, redirectUrl: any) {
    this.user = user;
  }

}
