import {Paragraph} from "./paragraph";

describe('ParagraphModel', () => {
  const onePhrase = 'Example phrase.';

  let paragraph: Paragraph;

  it('should contain nothing', () => {
    paragraph = new Paragraph();
    expect(paragraph.sentences.length).toBe(0);
    expect(paragraph.numWords).toBe(0);
    expect(paragraph.numChars).toBe(0);
  });

  it('should contain one phrase', () => {
    paragraph = new Paragraph();
    paragraph.text = onePhrase;
    paragraph.updateNumSentences();
    paragraph.updateNumWords();
    paragraph.updateNumChars();

    expect(paragraph.sentences.length).toBe(1);
    expect(paragraph.numWords).toBe(2);
    expect(paragraph.numChars).toBe(onePhrase.length);
  });

  it('should contain two phrases', () => {
    const twoPhrases = onePhrase + ' ' + onePhrase;
    paragraph = new Paragraph();
    paragraph.text = twoPhrases;
    paragraph.updateNumSentences();
    paragraph.updateNumWords();
    paragraph.updateNumChars();

    expect(paragraph.sentences.length).toBe(2);
    expect(paragraph.numWords).toBe(4);
    expect(paragraph.numChars).toBe(twoPhrases.length);
  });
});
