"use strict";
var Paragraph = (function () {
    function Paragraph() {
        this.text = "";
        this.sentences = [];
        this.numWords = 0;
        this.numChars = 0;
        this.minSentence = 0;
        this.maxSentence = 0;
        this.averageSentence = 0;
    }
    Paragraph.prototype.updateNumSentences = function () {
        var newNumSentences = 0;
        if (this.text) {
            var sentences = this.text.split(Paragraph.SENTENCE_PATTERN);
            this.sentences = sentences ? sentences.filter(function (sentence) { return sentence; }) : [];
            if (this.sentences) {
                var sizes = this.sentences.map(function (sentence) { return sentence.split(Paragraph.WORD_PATTERN).filter(function (word) { return word; }).length; });
                this.minSentence = Math.min.apply(Math, sizes);
                this.maxSentence = Math.max.apply(Math, sizes);
                this.averageSentence = Math.round(sizes.reduce(function (acc, val) { return acc + val; }, 0) / sizes.length);
            }
        }
    };
    Paragraph.prototype.updateNumWords = function () {
        var newNumWords = 0;
        if (this.text) {
            var words = this.text.split(Paragraph.WORD_PATTERN);
            if (words) {
                words = words.filter(function (word) { return word; });
            }
            newNumWords = words ? words.length : 0;
        }
        this.numWords = newNumWords;
    };
    Paragraph.prototype.updateNumChars = function () {
        this.numChars = this.text ? this.text.length : 0;
    };
    return Paragraph;
}());
Paragraph.SENTENCE_PATTERN = /[.?!]/g;
Paragraph.WORD_PATTERN = /\s/g;
exports.Paragraph = Paragraph;
//# sourceMappingURL=paragraph.js.map