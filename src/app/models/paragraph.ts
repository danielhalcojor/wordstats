export class Paragraph {
  static readonly SENTENCE_PATTERN = /[.?!]/g;
  static readonly WORD_PATTERN = /\s/g;

  text: string;
  sentences: string[];
  numWords: number;
  numChars: number;
  minSentence: number;
  maxSentence: number;
  averageSentence: number;

  constructor() {
    this.text = "";
    this.sentences = [];
    this.numWords = 0;
    this.numChars = 0;
    this.minSentence = 0;
    this.maxSentence = 0;
    this.averageSentence = 0;
  }

  updateNumSentences() {
    let newNumSentences = 0;
    if (this.text) {
      let sentences = this.text.split(Paragraph.SENTENCE_PATTERN);
      this.sentences = sentences ? sentences.filter(sentence => sentence) : [];

      if (this.sentences) {
        let sizes = this.sentences.map(sentence => sentence.split(Paragraph.WORD_PATTERN).filter(word => word).length);
        this.minSentence = Math.min(...sizes);
        this.maxSentence = Math.max(...sizes);
        this.averageSentence = Math.round(sizes.reduce((acc, val) => acc + val, 0) / sizes.length);
      }
    }
  }

  updateNumWords() {
    let newNumWords = 0;
    if (this.text) {
      let words = this.text.split(Paragraph.WORD_PATTERN);
      if (words) {
        words = words.filter(word => word);
      }
      newNumWords = words ? words.length : 0;
    }

    this.numWords = newNumWords;
  }

  updateNumChars() {
    this.numChars = this.text ? this.text.length : 0;
  }
}
