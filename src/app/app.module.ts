import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";

import {AppRoutingModule} from "./app-routing.module";

import { AppComponent }  from './components/app.component';
import {MainComponent} from "./components/main.component";
import {InfoComponent} from "./components/info.component";
import {ParagraphComponent} from "./components/paragraph.component";
import {SummaryComponent} from "./components/summary.component";
import {LoginComponent} from "./components/login.component";

import {FirebaseService} from "./services/firebase.service";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    MainComponent,
    InfoComponent,
    ParagraphComponent,
    SummaryComponent,
    LoginComponent
  ],
  providers: [
    FirebaseService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
