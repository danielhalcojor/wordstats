import {Injectable} from "@angular/core";
import * as firebase from "firebase";
const firebaseui = require('firebaseui').firebaseui;

@Injectable()
export class FirebaseService {

  static ui: any;
  static uiConfig: any;
  static signInCallback: any;

  static initFirebase() {
    // Initialize Firebase
    firebase.initializeApp({
      apiKey: "AIzaSyAjerGJCxZi9p5Ubz7S-S-cHwNxBtiGDVg",
      authDomain: "wordstats-53582.firebaseapp.com",
      databaseURL: "https://wordstats-53582.firebaseio.com",
      projectId: "wordstats-53582",
      storageBucket: "wordstats-53582.appspot.com",
      messagingSenderId: "596776828469"
    });
  }

  static initFirebaseUI() {
    // FirebaseUI config.
    this.uiConfig = {
      callbacks: {
        signInSuccess: (user: firebase.User, credential: any, redirectUrl: any) => {
          this.signInCallback(user, credential, redirectUrl);
          return false;
        }
      },
      signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        /*firebase.auth.FacebookAuthProvider.PROVIDER_ID,
         firebase.auth.TwitterAuthProvider.PROVIDER_ID,
         firebase.auth.GithubAuthProvider.PROVIDER_ID,*/
        firebase.auth.EmailAuthProvider.PROVIDER_ID
      ],
      // Terms of service url.
      tosUrl: '<your-tos-url>'
    };

    // Initialize the FirebaseUI Widget using Firebase.
    this.ui = new firebaseui.auth.AuthUI(firebase.auth());
  }

  static startFirebaseUI() {
    this.ui.start('#firebaseui-auth-container', this.uiConfig);
  }

  static signOut() {
    firebase.auth().signOut();
  }

  static setSignInCallback(fn: any) {
    this.signInCallback = fn;

    // Listen to change in auth state so it displays the correct UI for when
    // the user is signed in or not.
    firebase.auth().onAuthStateChanged((user: firebase.User) => {
      this.signInCallback(user);
    }, (error) => console.log(error));
  }
}
