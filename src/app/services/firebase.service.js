"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var firebase = require("firebase");
var firebaseui = require('firebaseui').firebaseui;
var FirebaseService = (function () {
    function FirebaseService() {
    }
    FirebaseService.initFirebase = function () {
        // Initialize Firebase
        firebase.initializeApp({
            apiKey: "AIzaSyAjerGJCxZi9p5Ubz7S-S-cHwNxBtiGDVg",
            authDomain: "wordstats-53582.firebaseapp.com",
            databaseURL: "https://wordstats-53582.firebaseio.com",
            projectId: "wordstats-53582",
            storageBucket: "wordstats-53582.appspot.com",
            messagingSenderId: "596776828469"
        });
    };
    FirebaseService.initFirebaseUI = function () {
        var _this = this;
        // FirebaseUI config.
        this.uiConfig = {
            callbacks: {
                signInSuccess: function (user, credential, redirectUrl) {
                    _this.signInCallback(user, credential, redirectUrl);
                    return false;
                }
            },
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                /*firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                 firebase.auth.TwitterAuthProvider.PROVIDER_ID,
                 firebase.auth.GithubAuthProvider.PROVIDER_ID,*/
                firebase.auth.EmailAuthProvider.PROVIDER_ID
            ],
            // Terms of service url.
            tosUrl: '<your-tos-url>'
        };
        // Initialize the FirebaseUI Widget using Firebase.
        this.ui = new firebaseui.auth.AuthUI(firebase.auth());
    };
    FirebaseService.startFirebaseUI = function () {
        this.ui.start('#firebaseui-auth-container', this.uiConfig);
    };
    FirebaseService.signOut = function () {
        firebase.auth().signOut();
    };
    FirebaseService.setSignInCallback = function (fn) {
        var _this = this;
        this.signInCallback = fn;
        // Listen to change in auth state so it displays the correct UI for when
        // the user is signed in or not.
        firebase.auth().onAuthStateChanged(function (user) {
            _this.signInCallback(user);
        });
    };
    return FirebaseService;
}());
FirebaseService = __decorate([
    core_1.Injectable()
], FirebaseService);
exports.FirebaseService = FirebaseService;
//# sourceMappingURL=firebase.service.js.map