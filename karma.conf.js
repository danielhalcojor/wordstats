module.exports = function(config) {
  const srcBase    = 'src/';       // transpiled app JS and map files
  const appBase = srcBase + 'app/';

  // Testing helpers (optional) are conventionally in a folder called `testing`
  const testingBase    = 'testing/'; // transpiled test JS and map files
  const testingSrcBase = 'testing/'; // test source TS files

  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-phantomjs-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-spec-reporter')
    ],

    client: {
      builtPaths: [srcBase, testingBase], // add more spec base paths as needed
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },

    customLaunchers: {
      // From the CLI. Not used here but interesting
      // chrome setup for travis CI using chromium
      Chrome_travis_ci: {
        base: 'Chrome',
        flags: ['--no-sandbox']
      }
    },

    files: [
      // System.js for module loading
      'node_modules/systemjs/dist/system.src.js',

      // Polyfills
      'node_modules/core-js/client/shim.js',

      // zone.js
      'node_modules/zone.js/dist/zone.js',
      'node_modules/zone.js/dist/long-stack-trace-zone.js',
      'node_modules/zone.js/dist/proxy.js',
      'node_modules/zone.js/dist/sync-test.js',
      'node_modules/zone.js/dist/jasmine-patch.js',
      'node_modules/zone.js/dist/async-test.js',
      'node_modules/zone.js/dist/fake-async-test.js',

      // jquery
      'node_modules/jquery/dist/jquery.js',

      // PhantomJS (and possibly others) might require it
      {pattern: 'node_modules/systemjs/dist/system-polyfills.js', included: false, watched: false},

      // RxJs
      { pattern: 'node_modules/rxjs/**/*.js', included: false, watched: false },
      { pattern: 'node_modules/rxjs/**/*.js.map', included: false, watched: false },

      // Paths loaded via module imports:
      // Angular itself
      { pattern: 'node_modules/@angular/**/*.js', included: false, watched: false },
      { pattern: 'node_modules/@angular/**/*.js.map', included: false, watched: false },

      { pattern: srcBase + '/systemjs.config.js', included: false, watched: false },
      { pattern: srcBase + '/systemjs.config.extras.js', included: false, watched: false },
      'karma-test-shim.js', // optionally extend SystemJS mapping e.g., with barrels

      // transpiled application & spec code paths loaded via module imports
      { pattern: srcBase + '**/*.js', included: false, watched: true },
      { pattern: testingBase + '**/*.js', included: false, watched: true },


      // Asset (HTML & CSS) paths loaded via Angular's component compiler
      // (these paths need to be rewritten, see proxies section)
      { pattern: srcBase + '**/*.html', included: false, watched: true },
      { pattern: srcBase + '**/*.css', included: false, watched: true },
      { pattern: srcBase + '**/*.png', included: false, watched: true },

      // Paths for debugging with source maps in dev tools
      { pattern: srcBase + '**/*.ts', included: false, watched: false },
      { pattern: srcBase + '**/*.js.map', included: false, watched: false },
      { pattern: testingSrcBase + '**/*.ts', included: false, watched: false },
      { pattern: testingBase + '**/*.js.map', included: false, watched: false}
    ],

    // Proxied base paths for loading assets
    proxies: {
      // required for modules fetched by SystemJS
      '/base/src/node_modules/': '/base/node_modules/',
      '/app/templates/': '/base/' + srcBase + 'app/templates/'
    },

    exclude: [],
    preprocessors: {},
    reporters: ['spec', 'kjhtml'],
    specReporter: {
      colors: true,
      maxLongLines: 5,
      supressFailed: false,
      supressPassed: false
    },

    phantomjsLauncher: {
      exitOnResourceError: true
    },

    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: false
  })
};
