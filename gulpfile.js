"use strict";

const gulp = require("gulp");
const plugins = require("gulp-load-plugins")();
const browserSync = require("browser-sync").create();
const browserSyncConf = require("./bs-config.js");
const del = require("del");
const tsProject = plugins.typescript.createProject("./tsconfig.json");

/**
 * Remove build directory.
 */
gulp.task('clean', () => {
  return del(["build"]);
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', () => {
  return gulp.src("src/**/*.ts")
    .pipe(plugins.tslint({
      formatter: 'prose'
    }))
    .pipe(plugins.tslint.report());
});

/**
 * Compile SASS scss sources into css
 */
gulp.task("sass", () => {
  return gulp
    .src("src/styles/*.scss")
    .pipe(plugins.sass.sync().on("error", plugins.sass.logError))
    .pipe(gulp.dest("src/styles/"));
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", ["clean", "tslint"], (cb) => {
  /*let tsResult = gulp.src(["src/!**!/!*.ts", "!src/!**!/!*.spec.ts"])
    .pipe(plugins.sourcemaps.init())
    .pipe(tsProject(plugins.typescript.reporter.defaultReporter()));
  return tsResult.js
    .pipe(plugins.sourcemaps.write(".", { sourceRoot: '/src' }))
    .pipe(gulp.dest("src"));*/
  plugins.exec("tsc -p src/");
  cb();
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", ["sass"], () => {
  return gulp
    .src(["src/**/*.html", "src/**/*.css", "!**/*.ts", "!**/*.spec.js"])
    .pipe(gulp.dest("src"));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', () => {
  gulp.watch(["src/**/*.ts"], ['compile']).on('change', function(e) {
    console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
  });
  gulp.watch(["src/**/*.html", "src/**/*.scss"], ['resources']).on('change', function(e) {
    console.log('Resource file ' + e.path + ' has been changed. Updating.');
  });
});

/**
 * Watch for changes in SASS files.
 */
gulp.task('watch-scss', () => {
  gulp.watch(["src/**/*.scss"], ['sass']).on('change', function(e) {
    console.log('Sass file ' + e.path + ' has been changed. Compiling.');
  });
});

/**
 * Start the development server. Configuration in bs-config.js file.
 */
gulp.task("dev", ["compile", "watch-scss", "watch"], () => {
  browserSync.init(browserSyncConf);
});
